#!/usr/bin/env python
import sys
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()

# for 3D plots:
# ax = fig.gca(projection='3d')

# show the legend
# plt.legend()

# plt.show()
